<%
  # /packages/intranet-dynfield/www/layout-page.adp
  # $Workfile: layout-page.adp $ $Revision: 1.7 $ $Date: 2013/04/18 16:22:56 $
%>
<master src="master">
<property name="doc(title)">@title;literal@</property>
<property name="context">@context;literal@</property>
<property name="left_navbar">@left_navbar_html;literal@</property>

<formtemplate id="page_layout"></formtemplate>
