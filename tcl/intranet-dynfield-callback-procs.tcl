# packages/intranet-dynfield/tcl/intranet-dynfield-callback-procs.tcl
ad_library {

  Callback procs for the intranet-dynfield package

  @author Malte Sussdorff (malte.sussdorff@cognovis.de)
  @creation-date 2004-09-28


}

ad_proc -public -callback im_category_after_create -impl intranet-dynfield {
	{-object_id:required}
	{-type ""}
	{-status ""}
	{-category_id ""}
	{-category_type ""}
	{-return_url ""}
} {
	Enable Editing permission for newly created categories
} {
	ns_log Notice "Running API im_category_after_create"

	set object_type [im_category_object_type -category_type $category_type]

	ns_log Notice "CATEGORY $category_id | $category_type" OBJECT TYPE: $object_type"

	if {[exists_and_not_null object_type] && [exists_and_not_null category_id]} {

		set lowest_object_type_id [db_list select_min_object_type_id {
			select min(object_type_id) from im_dynfield_type_attribute_map tam, im_categories ic where tam.object_type_id = ic.category_id and category_type = :category_type
		}]
	
		ns_log Notice "LOWEST OBJECT TYPE ID: $lowest_object_type_id"
		
		if {[exists_and_not_null lowest_object_type_id]} {

			set attribute_ids [db_list select_attribute_ids {select attribute_id from im_dynfield_type_attribute_map WHERE object_type_id = :lowest_object_type_id}]
	
			foreach attribute_id $attribute_ids {
				ns_log Notice "$attribute_id"
				db_dml insert_attribute_category_map {
					insert into im_dynfield_type_attribute_map (attribute_id, object_type_id, display_mode) VALUES (:attribute_id, :category_id, 'edit')
				}
			}
		}
	} 
}