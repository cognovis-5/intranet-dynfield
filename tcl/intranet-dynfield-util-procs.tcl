# packages/intranet-dynfield/tcl/intranet-dynfield-callback-procs.tcl
ad_library {

  Util procs for the intranet-dynfield package

  @author Malte Sussdorff (malte.sussdorff@cognovis.de)
  @creation-date 2019-08-03

}

ad_proc -public im_dynfield::sorted_attributes {
	-object_type:required
	{-object_type_id ""}
	{-page_url "default"}
	{-privilege ""}
	{-display_modes ""}
	{-user_id ""}
} {
	Returns the list of dynfield_attribute names for an object_type in the order given by the page_url

	@param object_type Object Type for which we return the attributes
	@param object_type_id Category ID of the subtype for which we want the dynfield attributes
	@param privilege Privilege for which we need to check. Default to "" which means we return all privileges
	@param page_url Page_Url from im_dynfield_layout which contains the sort_order
} {

	if {$object_type_id eq ""} {
		set attribute_sql "
				select aa.attribute_name, da.attribute_id
				from im_dynfield_attributes da, acs_attributes aa, im_dynfield_layout la
			where da.acs_attribute_id = aa.attribute_id
			and da.attribute_id = la.attribute_id
			and la.page_url = :page_url
			and object_type = :object_type
			order by pos_y"
	} else {
			if {$display_modes eq ""} {
					set display_where ""
			} else {
					if {[lsearch $display_modes "edit"]>-1} {
								# Make sure that for edit forms the user actually has write permission
						set privilege "write"
					}
					set display_where "and tam.display_mode in ([template::util::tcl_to_sql_list $display_modes])"
			}
		set attribute_sql "
				select aa.attribute_name, da.attribute_id
				from im_dynfield_attributes da, acs_attributes aa, im_dynfield_layout la, im_dynfield_type_attribute_map tam
			where da.acs_attribute_id = aa.attribute_id
			and da.attribute_id = la.attribute_id
			and la.page_url = :page_url
			and tam.object_type_id = :object_type_id
			and tam.attribute_id = da.attribute_id
			$display_where
			order by pos_y"
	}

	if {$user_id eq ""} {
		set user_id [ad_conn user_id]
	}
	set dynfield_attributes [list]
	db_foreach dynfield $attribute_sql {
		if {$privilege ne ""} {
			if {[im_object_permission -object_id $attribute_id -user_id $user_id -privilege $privilege]} {
				lappend dynfield_attributes $attribute_name
			}
		} else {
			lappend dynfield_attributes $attribute_name
		}
	}
	return $dynfield_attributes
}

ad_proc -public im_dynfield::sorted_attributes_selects {
		-object_type:required
		{-object_type_id ""}
		{-page_url "default"}
		{-privilege ""}
		{-display_modes ""}
		{-user_id ""}
		{-deref_p 0}
		{-exclude_tables ""}
} {
		Returns a list of selects, tables, outer_joins and wheres to retrieve the attributes

		@author Malte Sussdorff (malte.sussdorff@cognovis.de)
		@creation-date 2017-12-08
} {

		# Get the object type tables and id_columns
		db_1row object_type_info "select id_column, table_name,type_column,status_type_table from acs_object_types where object_type = :object_type"

		set array_val(object_type_column) $type_column
		set ref_column "${table_name}.${id_column}"
		set tables [list]
		set wheres [list]
		set selects [list]

		foreach type [ams::object_parents -object_type $object_type -hide_current] {
				db_1row object_type_info "select id_column, table_name as parent_table_name from acs_object_types where object_type = :type"
				if {[lsearch $exclude_tables $parent_table_name]<0} {
						lappend tables "$parent_table_name"
						lappend wheres "$ref_column = ${parent_table_name}.${id_column}"
				}
		}

		lappend tables $table_name

		# Deal with out join tables
		set outer_joins [list]
		db_foreach tables {select table_name, id_column from acs_object_type_tables where object_type = :object_type} {
				if {[lsearch $tables $table_name]<0 && [lsearch $exclude_tables $table_name]<0} {
						# We need an outer_join
						lappend outer_joins "LEFT OUTER JOIN $table_name ON ${table_name}.$id_column = $ref_column"
				}
		}

		set attribute_names [im_dynfield::sorted_attributes -object_type $object_type \
				-object_type_id $object_type_id \
				-page_url $page_url \
				-privilege $privilege \
				-display_modes $display_modes \
				-user_id $user_id]
		if {$attribute_names eq ""} {
				return ""
		} else {
				db_foreach column_list_sql "
						select  w.deref_plpgsql_function,
								aa.attribute_name,
								aa.table_name,
								w.widget
				from    im_dynfield_widgets w,
								im_dynfield_attributes a,
								acs_attributes aa
				where   a.widget_name = w.widget_name and
								a.acs_attribute_id = aa.attribute_id and
								aa.object_type = :object_type and
								aa.attribute_name in ([template::util::tcl_to_sql_list $attribute_names])
				"  {
						switch $widget {
								date {
										lappend selects "to_char(${table_name}.$attribute_name,'YYYY-MM-DD HH24:MI:SS') as $attribute_name"
								}
								timestamp - jq_datetimepicker {
										lappend selects "to_char(${table_name}.$attribute_name,'YYYY-MM-DD HH24:MI:SS') as $attribute_name"
								}
								richtext {
										lappend selects "${table_name}.$attribute_name"
								}
								default {
										if {$deref_plpgsql_function eq "" || $deref_p eq 0} {
												lappend selects "${table_name}.$attribute_name"
										} else {
												lappend selects "${deref_plpgsql_function}(${table_name}.$attribute_name) as ${attribute_name}_deref"
												lappend selects "${table_name}.$attribute_name"
												lappend attribute_names ${attribute_name}_deref
										}
								}
						}

				}
				return [list $attribute_names $selects $tables $outer_joins $wheres]
		}
}

ad_proc -public im_dynfield::object_array {
	{-array_name:required}
	{-object_id:required}
	{-publish_status "live"}
} {
	Sets an array in the calling environment with the values of the object.
	It takes all the possible values (so no filtering by object_type_id) and runs I18N over them

	@author Malte Sussdorff (malte.sussdorff@cognovis.de)
	@creation-date 2011-08-08

	@param array_name Name of the array which we are going to use
	@param object_id object_id for which we need to retrieve
	@param publish_status one of 'live', 'ready', or 'production'

	@error
} {

    upvar 1 $array_name array_val
    if { [info exists array_val] } {
	unset array_val
    }
    
    set object_type [acs_object_type $object_id]
    if {$object_type eq "user"} {
	set object_type "person"
    }
    
    # Get the object type tables and id_columns
    db_1row object_type_info "select id_column, table_name,type_column,status_type_table from acs_object_types where object_type = :object_type"
    
    set array_val(object_type_column) $type_column
    set ref_column "${table_name}.${id_column}"
    set tables [list]
    set wheres [list "$ref_column = $object_id"]
    set selects [list "${status_type_table}.$type_column"]

    lappend tables $table_name

	# Deal with out join tables
	set outer_joins ""
	db_foreach tables {select table_name, id_column from acs_object_type_tables where object_type = :object_type} {
				if {[lsearch $tables $table_name]<0} {
					# We need an outer_join
					append outer_joins "LEFT OUTER JOIN $table_name ON ${table_name}.$id_column = $ref_column"
				}
	}

	set attribute_names [list]
	set category_attribute_names [list]
	set deref_attribute_names [list]
	set date_attribute_names [list]
	set timestamp_attribute_names [list]
	set richtext_attribute_names [list]

	db_foreach column_list_sql {
		select  w.deref_plpgsql_function,
				aa.attribute_name,
				aa.table_name,
				w.widget
		from    im_dynfield_widgets w,
				im_dynfield_attributes a,
				acs_attributes aa
		where   a.widget_name = w.widget_name and
				a.acs_attribute_id = aa.attribute_id and
				aa.object_type = :object_type
	}  {
				switch $widget {
					im_category_tree {
								lappend selects "${table_name}.$attribute_name"
								lappend category_attribute_names $attribute_name
					}
					date {
								lappend selects "to_char(${table_name}.$attribute_name,'YYYY-MM-DD HH24:MI:SS') as $attribute_name"
								lappend date_attribute_names $attribute_name
						}
						timestamp - jq_datetimepicker {
						lappend selects "to_char(${table_name}.$attribute_name,'YYYY-MM-DD HH24:MI:SS') as $attribute_name"
						lappend timestamp_attribute_names $attribute_name
					}
					richtext {
								lappend selects "${table_name}.$attribute_name"
								lappend richtext_attribute_names $attribute_name
					}
					default {
								if {$deref_plpgsql_function eq ""} {
									lappend selects "${table_name}.$attribute_name"
									lappend attribute_names $attribute_name
								} else {
									lappend selects "${deref_plpgsql_function}(${table_name}.$attribute_name) as ${attribute_name}_deref, ${table_name}.$attribute_name"
									lappend deref_attribute_names $attribute_name
								}
					}
				}
	}

	# Retreive the data
	set sql "   select [join $selects ",\n"]
		from [join $tables ",\n"] $outer_joins
		WHERE [join $wheres "\n and "]"
	if { ![db_0or1row project_info_query "$sql"]
	 } {
		ad_return_complaint 1 "Cant Find $sql"
		return
	}

	# Now that the data is set, loop through the ns_set and put the
	# values into the array.

	foreach attribute_name $attribute_names {
				set array_val($attribute_name) [set $attribute_name]
	}

	foreach attribute_name $deref_attribute_names {
				set array_val($attribute_name) [set ${attribute_name}_deref]
				set array_val(${attribute_name}_orig) [set ${attribute_name}]
	}

	foreach attribute_name $date_attribute_names {
				set array_val($attribute_name) [lc_time_fmt [set $attribute_name] %q]
				set array_val(${attribute_name}_orig) [set ${attribute_name}]
	}

	foreach attribute_name $timestamp_attribute_names {
		if {[catch {
			set array_val($attribute_name) "[lc_time_fmt [set $attribute_name] "%q - %H:%M"]"
		}]} {
			set array_val($attribute_name) [set ${attribute_name}]
		}
				set array_val(${attribute_name}_orig) [set ${attribute_name}]
	}

	foreach attribute_name $category_attribute_names {
				set array_val($attribute_name) [im_category_from_id [set $attribute_name]]
				set array_val(${attribute_name}_orig) [set ${attribute_name}]
	}

	foreach attribute_name $richtext_attribute_names {
				# Make sure we don't run into issues if the attribute has been filled from outside ]project-open[
				# Without the formats
				set attribute_value [set $attribute_name]
				if { ![ad_html_text_convertable_p -from [lindex $attribute_value 1] -to "text/html"] } {
					# Whatever the string is, it is not convertible, so we don't event try
					set array_val(${attribute_name}) [set ${attribute_name}]
				} else {
					set array_val($attribute_name) [template::util::richtext::get_property html_value "[set $attribute_name]"]
				}
				set array_val(${attribute_name}_orig) [set ${attribute_name}]
	}

	set array_val(object_type_id) [set $type_column]
	return 1
}

ad_proc -public im_dynfield::dynfields {
    -object_type:required
    {-object_type_id ""}
    {-privilege ""}
    {-user_id ""}
} {
    Returns the list of dynfield_attributes for an object_type

    @param object_type Object Type for which we return the attributes
    @param object_type_id Category ID of the subtype for which we want the dynfield attributes
    @param privilege Privilege for which we need to check. Default to "" which means we return all privileges
} {
    if {$object_type_id eq ""} {
        set attribute_sql "
                select distinct da.attribute_id
                from im_dynfield_attributes da, acs_attributes aa
            where da.acs_attribute_id = aa.attribute_id
            and object_type = :object_type"
    } else {
        set attribute_sql "
            select distinct attribute_id
            from im_dynfield_type_attribute_map
            where object_type_id = :object_type_id"
    }

    if {$user_id eq ""} {
        set user_id [ad_conn user_id]
    }
    set dynfield_attributes [list]
    db_foreach dynfield $attribute_sql {
        if {$privilege ne ""} {
            if {[im_object_permission -object_id $attribute_id -user_id $user_id -privilege $privilege]} {
                lappend dynfield_attributes $attribute_id
            }
        } else {
            lappend dynfield_attributes $attribute_id
        }
    }
    return $dynfield_attributes
}
